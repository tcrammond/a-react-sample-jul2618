import React, { Component } from 'react';

import './App.css';

import Header from '../Header/Header';
import Footer from '../Footer/Footer';
import StoreItem from '../StoreItem/StoreItem';

// dev
const DEFAULT_ITEM = {
  title: 'Energy saving light bulb',
  subtitle: '25W // Pack of 4',
  photoURL: 'https://placekitten.com/300/300',

  price: 1299,
  isInStock: true,

  description: 'Available in 7 watts, 9 watts, 11 watts Spiral Light Bulb in B22, bulb switches on instantly, no waiting around warm start and flicker free features.  This is some extra text contained in the full description.',
  excerpt: 'Available in 7 watts, 9 watts, 11 watts Spiral Light Bulb in B22, bulb switches on instantly, no waiting around warm start and flicker free features...',
  specs: {
    brand: 'Philips',
    weight: '77g',
    dimensions: '12.6x6.2x6.2cm',
    model: 'E27 ES',
    colour: 'Cool daylight'
  },
  performanceUrl: ''
};

class App extends Component {
  render() {
    return (
      <div className="App">
        <main className="container">
          <Header />

          <StoreItem item={DEFAULT_ITEM} />

          <Footer />
        </main>

      </div>
    );
  }
}

export default App;
