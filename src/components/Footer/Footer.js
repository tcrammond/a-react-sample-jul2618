import React from 'react';

import './Footer.css';

function Footer () {
  return (
    <footer className="Footer section" role="contentinfo">
      <p>Octopus Energy Ltd is a company registered in England and Wales.</p>
      <p>Registered number: 09263424.Registered office: 33 Holborn, London, EC1N2HT.</p>
      <p>Trading office: 20-24 BroadwickStreet, London, W1F8HT</p>
    </footer>
  );
}

export default Footer;