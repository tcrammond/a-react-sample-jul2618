import React from 'react';

class ShowMore extends React.Component {
  state = {
    shown: false
  };

  toggleShow = () => {
    this.setState((state) => {
      return {
        shown: !state.shown
      };
    });
  }

  render () {
    return this.props.children({toggle: this.toggleShow, shown: this.state.shown});
  }
}

export default ShowMore;
