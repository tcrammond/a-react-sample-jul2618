import React from 'react';

import './Header.css';

import logo from './octopus-logo.svg';

function Header () {
  return (
    <header className="Header section">
      <img src={logo} alt="Octopus Energy Logo" />
    </header>
  );
}

export default Header;