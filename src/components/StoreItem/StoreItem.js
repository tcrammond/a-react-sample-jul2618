import React from 'react';

import './StoreItem.css';

import ShowMore from '../common/ShowMore';

// Dummy api call or similar.
const postToCart = () => {
  return new Promise((resolve) => {
    setTimeout(resolve, 1000);
  });
}

/**
 * Displays an item with all details and pricing, that can be added to the cart.
 */
class StoreItem extends React.PureComponent {
  state = {
    selectedQuantity: 1,

    isAddedToCart: false
  }

  changeQuantity = (value = 0) => {
    // Update the quantity in basket.
    // Let's quickly pretend we're triggering some async middleware or similar.
    // (it's messy as a result)

    if (this.state.isAddedToCart) {
      
      this.setState({
        isLoading: true
      }, () => {
        postToCart().then(() => {
          this.setState((state) => {
            const isRemoved = (state.selectedQuantity + value) === 0;
            return {
              selectedQuantity: state.selectedQuantity + value,
              isLoading: false,
              isAddedToCart: !isRemoved
            };
          });
        });
      });

      return;
    }
    
    this.setState((state) => {
      return {
        selectedQuantity: state.selectedQuantity + value
      };
    });
  }

  addToCart = () => {
    // let's quickly pretend we're triggering some async middleware or similar.
    this.setState({
      isLoading: true
    }, () => {
      postToCart().then(() => {
        this.setState({
          isAddedToCart: true,
          isLoading: false
        });
      });
    });
  }

  render () {
    const item = this.props.item;
    
    return (
      <article className="StoreItem">
        <div className="container">
          <div className="columns is-multiline">
  
            <div className="column is-6">
              <figure className="StoreItem__Photo">
                <img src={item.photoURL} alt={item.title}/>
              </figure>
            </div>
  
            <div className="column is-6">
              <StoreItemHeadline title={item.title} subtitle={item.subtitle} />
              <StoreItemPurchase
                price={item.price}
                selectedQuantity={this.state.selectedQuantity}
                isInStock={item.isInStock}
                isAddedToCart={this.state.isAddedToCart}
                isLoading={this.state.isLoading}
                onChangeQuantity={this.changeQuantity}
                onAddToCart={this.addToCart}
              />
            </div>
  
            <div className="column">
              <StoreItemDetails description={item.description} excerpt={item.excerpt} specs={item.specs} />
            </div>
          </div>
        </div>
      </article>
    );
  }
}

// I'd like to organize these better in different modules but it's a hassle for a demo.
function StoreItemHeadline ({ title, subtitle }) {
  return (
    <div className="StoreItem__Headline">
      <h1 className="StoreItem__HeadlineTitle">
        {title}
      </h1>
      <p className="StoreItem__HeadlineSpec">
        {subtitle}
      </p>
    </div>
  );
}

function StoreItemPurchase (props) {
  const {
    price,
    isAddedToCart,
    isLoading,
    selectedQuantity = 1,
    onChangeQuantity = () => {},
    onAddToCart = () => {}
  } = props;

  return (
    <div className="StoreItem__Purchase">
      <div className="StoreItem__PurchaseInfo">
        <div className="StoreItem__PurchasePrice">
          <ItemPrice price={price} />
        </div>

        <div className="StoreItem__PurchaseQuantity">
          <span className="subtitle">QTY</span>
          <button onClick={() => onChangeQuantity(-1)} disabled={isLoading} aria-label="Reduce quantity">
            -
          </button>
          <span className="currentQuantity" aria-label="Quantity" aria-live="assertive">{selectedQuantity}</span>
          <button onClick={() => onChangeQuantity(1)} disabled={isLoading} aria-label="Increase quantity">
            +
          </button>
        </div>
      </div>

      <button 
        className="StoreItem__AddToCart"
        disabled={isAddedToCart || isLoading}
        onClick={onAddToCart}
        aria-live="polite"
      >
        {isAddedToCart ? `${selectedQuantity} in your cart` : 'Add to cart'}
      </button>
      
    </div>
  )
}

const ItemPrice = ({ price }) => {
  const decimalPrice = (price / 100).toFixed(2);
  const [ pounds, pence ] = decimalPrice.split('.');

  return (
    <div aria-label="Price">
      <span>£{pounds}</span>
      <span>.{pence}</span>
    </div>
  );
}

function StoreItemDetails ({ description, excerpt, specs }) {
  return (
    <React.Fragment>
      <div className="StoreItem__Details">
        <div className="StoreItem__DetailsDescription">
          <h2>Description</h2>

          <ShowMore>
            {({toggle, shown}) => 
              <React.Fragment>
                <p className="Content">{shown ? description : excerpt}</p>
                <a onClick={toggle} tabIndex={0}>{shown ? 'Show less' : 'Show more'}</a>
              </React.Fragment>
            }
          </ShowMore>
        </div>
      </div>

      <div className="StoreItem__Specification">
        <h2>Specifications</h2>
        
        <ShowMore>
          {({toggle, shown}) => 
            <React.Fragment>
              {shown && <SpecificationsTable specs={specs} />}
              <a onClick={toggle} tabIndex={0}>{shown ? 'Show less' : 'Show more'}</a>
            </React.Fragment>
          }
        </ShowMore>
      </div>

      <div className="StoreItem__Performance">
        <h2>Performance</h2>
        [Graph]
      </div>
    </React.Fragment>
    
  );
}

function SpecificationsTable ({ specs = {}}) {
  return (
    <table>
      <tbody>
        {specs.brand && <tr><td>Brand</td><td>{specs.brand}</td></tr>}
        {specs.weight && <tr><td>Item weight</td><td>{specs.weight}</td></tr>}
        {specs.dimensions && <tr><td>Dimensions</td><td>{specs.dimensions}</td></tr>}
        {specs.model && <tr><td>Item model number</td><td>{specs.model}</td></tr>}
        {specs.colour && <tr><td>Colour</td><td>{specs.colour}</td></tr>}
      </tbody>
    </table>
  );
}

export default StoreItem;