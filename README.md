# octopus-energy

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

## Project notes

I've completed the following over ~3 hours or so:

* A reasonable replication of the mobile styles (developed with Chrome dev tools emulation of iPhone)
* A bare bones desktop responsive version - it has not received attention beyond the content layout changing
* The 'Show more' buttons in the content show/hide additional content (the actual use case set up here is not great)
* The quantity buttons and 'Add to cart' buttons pretend to talk to the server and update their displayed state.

It turned out I no longer had access to software to open a PSD correctly, so there has not been an effort to
have pixel perfect spacing or anything in this demo (as well as in the interest of time).

Libraries/tools used:

* React
* SCSS/SASS
* Some parts of Bulma css library - grid, reset, etc.

## Set up

Install dependencies

`npm install`

## Development

Run `npm run start`.

## Build / production mode

Run `npm run build`.

Builds the app for production to the build folder.

To quickly serve this on a local server (requires recent node LTS)

```
npm install -g serve
serve -s build
```